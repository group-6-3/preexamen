/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreExamen;

/**
 *
 * @author vitic
 */
public class Factura extends NotaVenta implements IVA{
    // Atributos
    private String RFC;
    private String nombreCliente;
    private String domicilioFiscal;
    
    // Constructores
    public Factura() {
        super();
        this.RFC = "";
        this.nombreCliente = "";
        this.domicilioFiscal = "";
    }

    public Factura(int numVenta, String fecha, String concepto, Producto producto, int cantidad, String tipoPago, String RFC, String nombreCliente, String domicilioFiscal) {
        super(numVenta, fecha, concepto, producto, cantidad, tipoPago);
        this.RFC = RFC;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
    }
    
    // Encapsulado
    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }
    
    // Métodos
    @Override
    public float calcularIVA(){
        return this.calcularPago() * 0.16f;
    }
}

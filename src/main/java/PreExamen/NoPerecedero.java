/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreExamen;

/**
 *
 * @author vitic
 */
public class NoPerecedero extends Producto{
    // Atributos
    private String loteFabricacion;
    
    // Constructores
    public NoPerecedero() {
        super();
        this.loteFabricacion = "";
    }

    public NoPerecedero(int idProducto, String nombreProducto, String unidadProducto, float precioUnitario, String loteFabricacion) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.loteFabricacion = loteFabricacion;
    }
    
// Encapsulado
    public String getLoteFabricacion() {    
        return loteFabricacion;
    }
    
    public void setLoteFabricacion(String loteFabricacion) {    
        this.loteFabricacion = loteFabricacion;
    }

    // Método
    @Override
    public float calcularPrecio() {
        return this.getPrecioUnitario() * 1.50f;
    }
}

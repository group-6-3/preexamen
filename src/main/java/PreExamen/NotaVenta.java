/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreExamen;

/**
 *
 * @author vitic
 */
public class NotaVenta {
    // Atributos
    protected int numVenta;
    protected String fecha;
    protected String concepto;
    protected Producto producto;
    protected int cantidad;
    protected String tipoPago;
    
    // Constructores
    public NotaVenta() {
        this.numVenta = 0;
        this.fecha = "";
        this.concepto = "";
        this.producto = null;
        this.cantidad = 0;
        this.tipoPago = "";
    }

    public NotaVenta(int numVenta, String fecha, String concepto, Producto producto, int cantidad, String tipoPago) {
        this.numVenta = numVenta;
        this.fecha = fecha;
        this.concepto = concepto;
        this.producto = producto;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }
    
    // Encapsulado
    public int getNumVenta() {
        return numVenta;
    }

    public void setNumVenta(int numVenta) {
        this.numVenta = numVenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    // Métodos
    public float calcularPago(){
        return this.cantidad * this.producto.getPrecioUnitario();
    }
}

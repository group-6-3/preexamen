/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreExamen;

/**
 *
 * @author vitic
 */
public class Perecedero extends Producto{
    // Atributos
    private String fechaCaducidad;
    private float temperatura;
    
    // Constructores
    public Perecedero() {
        super();
        this.fechaCaducidad = "";
        this.temperatura = 0.0f;
    }

    public Perecedero(int idProducto, String nombreProducto, String unidadProducto, float precioUnitario, String fechaCaducidad, float temperatura) {
        super(idProducto, nombreProducto, unidadProducto, precioUnitario);
        this.fechaCaducidad = fechaCaducidad;
        this.temperatura = temperatura;
    }
    
    //Encapsulado
    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }
    
    // Método
    @Override
    public float calcularPrecio() {
        String unidad = this.getUnidadProducto();
        float incremento = 1.0f;
        
        switch(unidad){
            case "KG": incremento = 1.03f; break;
            case "Litros": incremento = 1.05f; break;
            case "Pieza": incremento = 1.04f; break;
        }
        
        return (this.getPrecioUnitario() * incremento) * 1.50f;
    }
}

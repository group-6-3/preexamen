/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreExamen;

/**
 *
 * @author vitic
 */
public abstract class Producto {
    // Atributos
    protected int idProducto;
    protected String nombreProducto;
    protected String unidadProducto;
    protected float precioUnitario;
    
    // Constructores
    public Producto() {
        this.idProducto = 0;
        this.nombreProducto = "";
        this.unidadProducto = "";
        this.precioUnitario = 0.0f;
    }

    public Producto(int idProducto, String nombreProducto, String unidadProducto, float precioUnitario) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.unidadProducto = unidadProducto;
        this.precioUnitario = precioUnitario;
    }
    
    // Encapsulado
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getUnidadProducto() {
        return unidadProducto;
    }

    public void setUnidadProducto(String unidadProducto) {
        this.unidadProducto = unidadProducto;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
    // Métodos
    public abstract float calcularPrecio();
}
